import { store } from "./store/configureStore";
import { createUser, editUserName, editUserAge, addUserDog, setUserDogMood } from "./store/actions/actions";

store.dispatch(createUser());
store.dispatch(createUser());

const firstUser = store.getState().users.toArray()[0][1];
const targetUserId = firstUser.id;

console.log(`Target user ${targetUserId}`);

store.dispatch(editUserName(targetUserId, "Shawn"));
store.dispatch(editUserAge(targetUserId, 22));
store.dispatch(addUserDog(targetUserId));
store.dispatch(setUserDogMood(targetUserId, 0, "happy"));
