import { ActionCreator, Action } from "redux";
import { IPayloadIdAction, ActionTypes, IPayloadIdKeyAction } from "../../../types/actionTypes";

export const createUser: ActionCreator<Action> = () => {
    return {
        type: ActionTypes.CREATE_USER,
    };
};

export const editUserName: ActionCreator<IPayloadIdAction<string>> =
(
    id: string,
    newName: string,
) => {
    return {
        type: ActionTypes.EDIT_USER_NAME,
        id,
        payload: newName,
    };
};

export const editUserAge: ActionCreator<IPayloadIdAction<number>> =
(
    id: string,
    newAge: number,
) => {
    return {
        type: ActionTypes.EDIT_USER_AGE,
        id,
        payload: newAge,
    };
};

export const addUserDog: ActionCreator<IPayloadIdAction<undefined>> =
(
    id: string,
) => {
    return {
        type: ActionTypes.ADD_USER_DOG,
        id,
        payload: undefined,
    };
};

export const setUserDogMood: ActionCreator<IPayloadIdKeyAction<string>> =
(
    id: string,
    key: number,
    newMood: string,
) => {
    return {
        type: ActionTypes.SET_USER_DOG_MOOD,
        id,
        key,
        payload: newMood,
    };
};
