import {createStore, Store} from "redux";
import { IApplicationState } from "../../types/types";
import { reducer } from "./reducers/reducer";

export const store: Store<IApplicationState> = createStore(reducer);

const logStata = () => console.log(JSON.stringify(store.getState(), undefined, 2));

logStata();

store.subscribe(() => {
    logStata();
});
