import { IUser } from "../../../types/types";
import { Action } from "redux";

import { Map } from "immutable";
import { ActionTypes, IPayloadIdAction, IPayloadIdKeyAction } from "../../../types/actionTypes";
import { getEmptyUser } from "../helpers/userHelper";
import { getEmptyDog } from "../helpers/dogHelper";

export const initialState = {
    users: Map<string, IUser>(),
};

export const reducer =
(
    state = initialState,
    action: Action,
) => {
    switch (action.type) {
        case ActionTypes.CREATE_USER: {
            const newUser = getEmptyUser();
            const users = state.users.set(newUser.id, newUser);
            return {
                ...state,
                users,
            };
        }

        case ActionTypes.EDIT_USER_NAME: {
            const nameIdAction = action as IPayloadIdAction<string>;
            const targetUser = state.users.get(nameIdAction.id);
            targetUser.name = nameIdAction.payload;
            const users = state.users.set(targetUser.id, targetUser);
            return {
                ...state,
                users,
            };
        }

        case ActionTypes.EDIT_USER_AGE: {
            const ageIdAction = action as IPayloadIdAction<number>;
            const targetUser = state.users.get(ageIdAction.id);
            targetUser.age = ageIdAction.payload;
            const users = state.users.set(targetUser.id, targetUser);
            return {
                ...state,
                users,
            };

        }

        case ActionTypes.ADD_USER_DOG: {
            const idAction = action as IPayloadIdAction<undefined>;
            const targetUser = state.users.get(idAction.id);
            const newDog = getEmptyDog();
            targetUser.dogs = targetUser.dogs.push(newDog);
            const users = state.users.set(targetUser.id, targetUser);
            return {
                ...state,
                users,
            };
        }

        case ActionTypes.SET_USER_DOG_MOOD: {
            const moodIdAction = action as IPayloadIdKeyAction<string>;
            const targetUser = state.users.get(moodIdAction.id);
            const targetDog = targetUser.dogs.get(moodIdAction.key);
            targetDog.mood = moodIdAction.payload;
            targetUser.dogs = targetUser.dogs.set(moodIdAction.key, targetDog);
            const users = state.users.set(moodIdAction.id, targetUser);
            return {
                ...state,
                users,
            };
        }

        default: {
            return state;
        }
    }
};
