import { IDog } from "../../../types/types";

export const getEmptyDog = (): IDog => ({
    nickName: "Violet",
    mood: "OK",
});
