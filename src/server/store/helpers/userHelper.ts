import { IUser, IDog } from "../../../types/types";
import {v4} from "uuid";
import {List} from "immutable";

export const getEmptyUser = (): IUser => ({
    id: v4(),
    name: "John",
    age: 0,
    dogs: List<IDog>(),
});
