import { Action } from "redux";

export enum ActionTypes {
    CREATE_USER,
    EDIT_USER_NAME,
    EDIT_USER_AGE,
    ADD_USER_DOG,
    SET_USER_DOG_MOOD,
}

export interface IPayloadAction<P> extends Action {
    payload: P;
}

export interface IPayloadIdAction<P> extends IPayloadAction<P> {
    id: string;
}

export interface IPayloadIdKeyAction<P> extends IPayloadIdAction<P> {
    key: number;
}
