import { List, Map } from "immutable";

export interface IDog {
    nickName: string;
    mood: string;
}

export interface IUser {
    id: string;
    name: string;
    age: number;
    dogs: List<IDog>;
}

// export type IUsers = { [userId: number]: IUser };

export interface IApplicationState {
    users: Map<string, IUser>;
}
